# README #

## Get Started ##
* Install Unreal Engine 4.9.0
* Run "DinoHunt.uproject" file

## Where to find what ##
* Level Maps in *Content/Maps* folder
* Level images in *Content/Textures/Levels* folder
* Other images in different folder under *Content/Textures*